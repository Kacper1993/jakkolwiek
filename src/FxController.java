import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class FxController {
    @FXML
    private PasswordField password;
    @FXML
    private TextField login;
    @FXML
    private Button button;

    public void passLoginAndPassword(){
        System.out.println("Login: "+login.getText());
        System.out.println("Password: "+password.getText());
    }

    public void keyPress(KeyEvent keyEvent) {
//        System.out.println(keyEvent.getCode());
        if(keyEvent.getCode().equals("ENTER")){
            passLoginAndPassword();
        }
    }
}
